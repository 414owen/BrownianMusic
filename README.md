# Brownian Music

See the demo [here](https://owen.cafe/brownianmusic).

If you look closely enough, everything is physics, but physics is just maths,  
maths is just philosophy, and philosophy is just a music discovery interface.
